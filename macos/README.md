## Moved to the Mac App Store

We've moved our Mac Desktop App to the Mac App Store. Download [here](https://apps.apple.com/us/app/skiff-desktop/id1615488683).

![Skiff Desktop in the Mac App Store](SkiffDesktopAppStore.png)
