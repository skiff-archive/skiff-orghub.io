
![Companies training AI on user data](https://github.com/skiff-org/skiff-org.github.io/blob/main/blogs/aiblog.png?raw=true)

## Current list

-   **X/Twitter**: As recently as September 2023, X confirmed that it would use "public data" to train machine learning [models](https://techcrunch.com/2023/09/01/xs-privacy-policy-confirms-it-will-use-public-data-to-train-ai-models/). X's CEO Elon Musk noted that the company would not use "DMs or anything private." 

-   **Zoom**: In March 2023, Zoom updated its terms of service to allow the company to use user data to train AI models. The update has sparked intense media coverage and controversy, as it gave Zoom broad control over user data without clear consent requirements. Zoom later backtracked on the update, saying that it would not use user data to train AI models without consent. Read more in [The Washington](https://www.washingtonpost.com/politics/2023/08/08/zooms-privacy-tweaks-stoke-fears-that-its-calls-will-be-used-train-ai/) [Post](https://www.washingtonpost.com/politics/2023/08/08/zooms-privacy-tweaks-stoke-fears-that-its-calls-will-be-used-train-ai/).

-   **Google**: In January 2023, Google updated its privacy policy to state that it may use user data to train AI models. The update was met with criticism from privacy advocates, who argued that Google was not being transparent about its data collection practices. Google has since clarified that it will only use user data to train AI models that are necessary for its core products and services. Read more in [9to5Google](https://9to5google.com/2023/07/03/google-privacy-policy-ai-training-data/).

-   **Facebook**: Facebook has an established history of using public data - including photos on Instagram and videos on Facebook - to train AI models. Read more in [CNBC](https://www.cnbc.com/2021/03/04/facebook-trains-ai-to-see-using-1-billion-public-instagram-photos-.html) or [The Verge](https://www.theverge.com/2021/3/12/22326975/facebook-training-ai-public-videos-digital-memories).

-  **Apple**: Apple has generally toed a line between training ML models and indiscriminately harnessing user data. Recently, Spotify stopped allowing Apple to train on podcast data (read more in [WIRED](https://www.wired.com/story/apple-spotify-audiobook-narrators-ai-contract/)).

-   **Getty Images**: Although Getty Images does not train AI models of their own (that we know of), they filed a lawsuit against Stability AI alleging that over 10 million photos were illegally used in AI training. Read more in [The Verge](https://www.theverge.com/2023/2/6/23587393/ai-art-copyright-lawsuit-getty-images-stable-diffusion).

-   **ChatGPT/OpenAI**: Unsurprisingly, user interactions with ChatGPT are used to improve the chatbot's service. However, in a prior version of OpenAI's API terms of service, the company could use developer-provided API requests as training data as well. OpenAI later changed this policy, as developers objected to their API calls being used as training data by OpenAI. Read more in [TechCrunch](https://techcrunch.com/2023/03/01/addressing-criticism-openai-will-no-longer-use-customer-data-to-train-its-models-by-default/).

-   **Snapchat**: At the very least, Snapchat is known to use conversations with "My AI" - it's AI chatbot - to train its AI models. Read more [on ZDNet](https://www.zdnet.com/article/do-you-use-snapchats-ai-chatbot-heres-the-data-its-pulling-from-you/). Snapchat also allegedly uses data from [Memories](https://dot.la/what-data-does-snapchat-collect-2658631894.html) to train models capable of recognizing user content.
## How to contribute

Open up a pull request and include a citation. Tag @amilich to review.
